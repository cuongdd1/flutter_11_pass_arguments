import 'package:flutter/material.dart';

class CommonUI extends StatelessWidget {
  const CommonUI({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('CommonUI'),
      ),
      body: const StackPage(),
    );
  }
}

class StackPage extends StatelessWidget {
  const StackPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Positioned(
          top: 16,
          left: 32,
          child: Container(
            width: 200,
            height: 200,
            color: Colors.red,
          ),
        ),
        Positioned(
          top: 32,
          left: 48,
          child: Container(
            width: 200,
            height: 200,
            color: Colors.yellow,
          ),
        ),
        Positioned(
          top: 48,
          left: 64,
          child: Container(
            width: 200,
            height: 200,
            color: Colors.green,
          ),
        ),
      ],
    );
  }
}
