import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';

class RandomWords extends StatefulWidget {
  const RandomWords({Key? key}) : super(key: key);

  static const routeName = 'RandomWords';

  @override
  _RandomWordsState createState() => _RandomWordsState();
}

class _RandomWordsState extends State<RandomWords> {
  List<WordPair> _suggestions = <WordPair>[];
  List<WordPair> _foundWords = <WordPair>[];
  final _biggerFont = const TextStyle(fontSize: 18.0);

  int get totalList => _foundWords.length;

  @override
  void initState() {
    getDataSource();
    super.initState();
  }

  Widget _buildSuggestions() {
    return ListView.builder(
      itemCount: _foundWords.length,
      itemBuilder: (context, index) => Card(
        color: Colors.blueAccent,
        elevation: 8,
        margin: const EdgeInsets.symmetric(vertical: 8),
        child: cellForRowsAt(index),
      ),
    );
  }

  Widget cellForRowsAt(int index) {
    return ListTile(
      leading: Text(
        (index + 1).toString(),
        style: _biggerFont,
      ),
      title: Text(
        _foundWords[index].first,
        style: _biggerFont,
      ),
      subtitle: Text(
        _foundWords[index].second.toString(),
        style: _biggerFont,
      ),
    );
  }

  void _runFilter(String text) {
    final keyWord = text.toLowerCase();
    _foundWords = _suggestions.where((wordPair) {
      return wordPair.first.contains(keyWord) ||
          wordPair.second.contains(keyWord);
    }).toList();

    setState(() {});
  }

  void getDataSource() {
    _suggestions = generateWordPairs().take(250).toList();
    _foundWords = _suggestions;
  }

  Widget _wordNotFound() {
    return const Center(
      child: Text('Word not found'),
    );
  }

  Widget _searchTextField() {
    return TextFormField(
      onChanged: (value) => _runFilter(value),
      decoration: const InputDecoration(
        icon: Icon(Icons.search),
        hintText: 'typing some word',
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'English Words',
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          children: <Widget>[
            _searchTextField(),
            const SizedBox(
              height: 16,
            ),
            Expanded(
                child: (_foundWords.isEmpty)
                    ? _wordNotFound()
                    : _buildSuggestions()),
            Text('$totalList'),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    print('dispose');
  }
}
