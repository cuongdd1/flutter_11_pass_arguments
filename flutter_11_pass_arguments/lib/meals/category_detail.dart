import 'package:flutter/material.dart';
import 'package:flutter_11_pass_arguments/models/categories_model.dart';
import 'package:transparent_image/transparent_image.dart';

class CategoryDetail extends StatefulWidget {
  const CategoryDetail({Key? key}) : super(key: key);

  static const routeName = 'CategoryDetail';

  @override
  _CategoryDetailState createState() => _CategoryDetailState();
}

class _CategoryDetailState extends State<CategoryDetail> {
  @override
  Widget build(BuildContext context) {
    final category =
        ModalRoute.of(context)!.settings.arguments as CategoryModel;
    return Scaffold(
      appBar: AppBar(
        title: Text('${category.strCategory}'),
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: ListView.builder(
          itemCount: 2,
          itemBuilder: (_, index) {
            if (index == 0) {
              return FadeInImage.memoryNetwork(
                placeholder: kTransparentImage,
                image: '${category.strCategoryThumb}',
              );
            } else {
              return Text(
                '${category.strCategoryDescription}',
                style: const TextStyle(fontSize: 18),
              );
            }
          },
        ),
      ),
    );
  }
}
