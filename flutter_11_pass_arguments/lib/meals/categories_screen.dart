import 'package:flutter/material.dart';
import 'package:flutter_11_pass_arguments/meals/category_detail.dart';
import 'package:flutter_11_pass_arguments/models/categories_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter_11_pass_arguments/services/categories_api.dart';
import 'package:shimmer/shimmer.dart';

class CategoriesScreen extends StatefulWidget {
  const CategoriesScreen({Key? key}) : super(key: key);

  static const routeName = 'CategoriesScreen';

  @override
  _CategoriesScreenState createState() => _CategoriesScreenState();
}

class _CategoriesScreenState extends State<CategoriesScreen> {
  List<CategoryModel> categories = [];

  @override
  void initState() {
    super.initState();
  }

  FutureBuilder<MealsModel> _buildBody(BuildContext context) {
    final client =
        RestClient(Dio(BaseOptions(contentType: "application/json")));
    return FutureBuilder<MealsModel>(
      future: client.getTasks(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          final List<CategoryModel> categories =
              snapshot.data?.categories ?? [];

          return ListView.builder(
            itemCount: categories.length,
            itemBuilder: (context, index) => ListTile(
              onTap: () {
                Navigator.pushNamed(context, CategoryDetail.routeName, arguments: categories[index]);
              },
              leading: Image.network(
                '${categories[index].strCategoryThumb}',
                width: 64,
                height: 64,
              ),
              title: Text('${categories[index].strCategory}'),
            ),
          );
        } else {
          return _showLoading();
        }
      },
    );
  }

  Widget _showLoading() {
    return Container(
      padding: const EdgeInsets.all(16),
      child: Expanded(
        child: Shimmer.fromColors(
          baseColor: Colors.grey[300]!,
          highlightColor: Colors.grey[100]!,
          enabled: true,
          child: ListView.builder(
            itemBuilder: (_, __) => Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    width: 48.0,
                    height: 48.0,
                    color: Colors.white,
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 8.0),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          width: double.infinity,
                          height: 8.0,
                          color: Colors.white,
                        ),
                        const Padding(
                          padding: EdgeInsets.symmetric(vertical: 2.0),
                        ),
                        Container(
                          width: double.infinity,
                          height: 8.0,
                          color: Colors.white,
                        ),
                        const Padding(
                          padding: EdgeInsets.symmetric(vertical: 2.0),
                        ),
                        Container(
                          width: 40.0,
                          height: 8.0,
                          color: Colors.white,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            itemCount: 16,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Categories'),
      ),
      body: _buildBody(context),
    );
  }
}
