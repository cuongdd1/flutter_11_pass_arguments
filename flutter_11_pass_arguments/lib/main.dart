import 'package:flutter/material.dart';
import 'package:flutter_11_pass_arguments/common_ui.dart';
import 'package:flutter_11_pass_arguments/meals/categories_screen.dart';
import 'package:flutter_11_pass_arguments/meals/category_detail.dart';
import 'package:flutter_11_pass_arguments/random_words.dart';
import 'package:flutter_11_pass_arguments/profile.dart';
import 'package:flutter_11_pass_arguments/search_user.dart';

void main() => runApp(const MyApp());

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  // @override
  // void initState() {
  //   // TODO: implement initState
  //   super.initState();
  // }
  //
  // @override
  // void didChangeAppLifecycleState(AppLifecycleState state) {
  //   if (state == AppLifecycleState.resumed) {
  //     //do your stuff
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // Register the widget in the routes table
      initialRoute: HomeScreen.routeName,
      routes: {
        HomeScreen.routeName: (context) => const HomeScreen(),
        ExtractArgumentsScreen.routeName: (context) =>
            const ExtractArgumentsScreen(),
        ProfileScreen.routeName: (context) => const ProfileScreen(),
        RandomWords.routeName: (context) => const RandomWords(),
        SearchUserPage.routeName: (context) => const SearchUserPage(),
        CategoriesScreen.routeName: (context) => const CategoriesScreen(),
        CategoryDetail.routeName: (context) => const CategoryDetail(),
      },

      onGenerateRoute: (settings) {
        // If you push the PassArguments route
        if (settings.name == PassArgumentsScreen.routeName) {
          final args = settings.arguments as ScreenArguments;
          return MaterialPageRoute(
            builder: (context) {
              return PassArgumentsScreen(
                title: args.title,
                message: args.message,
              );
            },
          );
        }
        assert(false, 'Need to implement ${settings.name}');
        return null;
      },
      title: 'Navigation with Arguments',
      home: const HomeScreen(),
    );
  }
}

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  static const routeName = 'HomeScreen';

  // Chuyển màn hình:
  void _pushToCommonUI(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const CommonUI()),
    );
  }

  void pushPassArgumentsScreen(BuildContext context) {
    Navigator.pushNamed(
      context,
      PassArgumentsScreen.routeName,
      arguments: ScreenArguments(
        'this is title',
        'this is message',
      ),
    );
  }

  Widget _createElevatedButton(
      BuildContext context, String title, ActionsButton actionsButton) {
    final _styleFromElevatedButton = ElevatedButton.styleFrom(
      minimumSize: const Size.fromHeight(
          40), // fromHeight use double.infinity as width and 40 is the height
    );
    return ElevatedButton(
      style: _styleFromElevatedButton,
      onPressed: () => _actionsForButtons(context, actionsButton),
      child: Text(
        title,
      ),
    );
  }

  void _actionsForButtons(BuildContext context, ActionsButton actionsButton) {
    switch (actionsButton) {
      case (ActionsButton.showWord):
        pushNamedToScreen(context, RandomWords.routeName, null);
        break;
      case (ActionsButton.gotoProfile):
        pushNamedToScreen(context, ProfileScreen.routeName, null);
        break;
      case (ActionsButton.extracts):
        ScreenArguments arguments = ScreenArguments(
          'Extract Arguments Screen',
          'This message is extracted in the build method.',
        );
        pushNamedToScreen(context, ExtractArgumentsScreen.routeName, arguments);
        break;
      case (ActionsButton.accepts):
        pushPassArgumentsScreen(context);
        break;
      case (ActionsButton.searchUsers):
        pushNamedToScreen(context, SearchUserPage.routeName, null);
        break;
      case (ActionsButton.mealScreen):
        pushNamedToScreen(context, CategoriesScreen.routeName, null);
        break;
      default:
        break;
    }
  }

  void pushNamedToScreen(
      BuildContext context, String routeName, Object? arguments) {
    Navigator.pushNamed(
      context,
      routeName,
      arguments: arguments,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home Screen'),
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _createElevatedButton(
                context, 'Show Words', ActionsButton.showWord),
            _createElevatedButton(
                context, 'Goto Profile', ActionsButton.gotoProfile),
            _createElevatedButton(
                context,
                'Navigate to screen that extracts arguments',
                ActionsButton.extracts),
            _createElevatedButton(
                context,
                'Navigate to a named that accepts arguments',
                ActionsButton.accepts),
            _createElevatedButton(
                context, 'search user simple', ActionsButton.searchUsers),
            _createElevatedButton(
                context, 'Categories Meal', ActionsButton.mealScreen),
            commonUIElevatedButton(context),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Expanded(
                  child: ElevatedButton(
                    onPressed: () {},
                    child: const Text('Approve'),
                  ),
                ),
                Expanded(
                  child: ElevatedButton(
                    onPressed: () {},
                    child: const Text('Reject'),
                  ),
                ),
                Expanded(
                  child: ElevatedButton(
                    onPressed: () {},
                    child: const Text('Need Revise'),
                  ),
                ),
              ],
            ),
            Expanded(
              child: ListView.builder(
                itemCount: 99,
                itemBuilder: (_, index) => Card(
                  color: Colors.red,
                  elevation: 8,
                  child: ListTile(
                    onTap: () {
                      TestStream().main();
                    },
                    title: Text('$index'),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget commonUIElevatedButton(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        _pushToCommonUI(context);
      },
      child: const Text('Common UI'),
      style: ElevatedButton.styleFrom(
        minimumSize: const Size.fromHeight(
            40), // fromHeight use double.infinity as width and 40 is the height
      ),
    );
  }
}

class TestStream {
  Future<int> sumStream(Stream<int> stream) async {
    print('sumStream start $stream');
    var sum = 0;
    await for (final value in stream) {
      print('sumStream in for $value');
      sum += value;
    }
    print('sumStream end $stream');
    return sum;
  }

  Stream<int> countStream(int to) async* {
    print('countStream start');
    for (int i = 1; i <= to; i++) {
      print('countStream in for $i');
      yield i;
    }
    print('countStream end');
  }

  Future<void> main() async {
    var stream = countStream(10);
    var sum = await sumStream(stream);
    print(sum); // 55
  }
}

// A Widget that extracts the necessary arguments from
// the ModalRoute.
class ExtractArgumentsScreen extends StatelessWidget {
  const ExtractArgumentsScreen({Key? key}) : super(key: key);

  static const routeName = '/extractArguments';

  @override
  Widget build(BuildContext context) {
    // Extract the arguments from the current ModalRoute
    // settings and cast them as ScreenArguments.
    final args = ModalRoute.of(context)!.settings.arguments as ScreenArguments;

    return Scaffold(
      appBar: AppBar(
        title: Text(args.title),
      ),
      body: Center(
        child: Text(args.message),
      ),
    );
  }
}

// A Widget that accepts the necessary arguments via the
// constructor.
class PassArgumentsScreen extends StatelessWidget {
  static const routeName = '/passArguments';

  final String title;
  final String message;

  const PassArgumentsScreen({
    Key? key,
    required this.title,
    required this.message,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
        child: Text(message),
      ),
    );
  }
}

class ScreenArguments {
  final String title;
  final String message;

  ScreenArguments(this.title, this.message);
}

enum ActionsButton {
  showWord,
  gotoProfile,
  extracts,
  accepts,
  searchUsers,
  commonUI,
  mealScreen,
}
