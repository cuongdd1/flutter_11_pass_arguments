import 'package:english_words/english_words.dart';
import 'package:flutter/material.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  static const routeName = 'ProfileScreen';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Profile',
        ),
      ),
      body: RegisterUser(),
    );
  }
}

class RegisterUser extends StatelessWidget {
  const RegisterUser({Key? key}) : super(key: key);

  void textDidChanged(String type, String text) {
    print(type);
    print(text);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      child: Column(
        children: <Widget>[
          TextFormField(
            decoration: const InputDecoration(
              icon: Icon(Icons.email),
              hintText: 'example@gmail.com',
              labelText: 'Email',
            ),
            onChanged: (value) => textDidChanged('email', value),
          ),
          TextFormField(
            obscureText: true,
            onChanged: (value) => textDidChanged('password', value),
            decoration: const InputDecoration(
              icon: Icon(Icons.lock),
              labelText: 'Password',
            ),
          ),
          SizedBox(
            height: 16.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              RaisedButton(
                onPressed: () {},
                color: Colors.blue,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Text(
                  'Login',
                  style: TextStyle(color: Colors.white),
                ),
              ),
              SizedBox(
                width: 16,
              ),
              RaisedButton(
                onPressed: () {},
                color: Colors.blue,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Text(
                  'Forgot Password',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 16.0,
          ),
          Text(
            'Or Login with',
            style: TextStyle(
              fontSize: 18.0,
            ),
          ),
          SizedBox(
            height: 16.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FacebookButon(),
              SizedBox(
                width: 16.0,
              ),
              GoogleButon(),
            ],
          ),
        ],
      ),
    );
  }
}

class FacebookButon extends StatelessWidget {
  const FacebookButon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: RaisedButton.icon(
        onPressed: () {
          print('Facebook Clicked.');
        },
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        label: Text(
          'Facebook',
          style: TextStyle(color: Colors.white),
        ),
        icon: Icon(
          Icons.facebook,
          color: Colors.white,
        ),
        textColor: Colors.white,
        splashColor: Colors.cyanAccent,
        color: Colors.blue,
      ),
    );
  }
}

class GoogleButon extends StatelessWidget {
  const GoogleButon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: RaisedButton.icon(
        onPressed: () {
          print('Facebook Clicked.');
        },
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        label: Text(
          'Google',
          style: TextStyle(color: Colors.white),
        ),
        icon: Icon(
          Icons.g_mobiledata,
          color: Colors.white,
        ),
        textColor: Colors.white,
        splashColor: Colors.cyanAccent,
        color: Colors.blue,
      ),
    );
  }
}
