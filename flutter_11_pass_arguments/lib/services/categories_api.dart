import 'package:flutter_11_pass_arguments/models/categories_model.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';

part 'categories_api.g.dart';

@RestApi(baseUrl: "https://themealdb.com/api/json/v1/1/categories.php")
abstract class RestClient {
  factory RestClient(Dio dio) = _RestClient;

  @GET("/posts")
  Future<MealsModel> getTasks();
}
